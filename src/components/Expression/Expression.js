import React from 'react';
import './Expression.css';

const input = props => (
	<div className="input">
		{ props.children }
	</div>
);

export default input;