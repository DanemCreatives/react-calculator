import React from 'react';
import './Button.css';

const button = props => (
	<div className="button" 
		// pass the number value to the click event handler
		onClick={() => props.clickEventHandler(props.children)}>
		{ props.children }
	</div>
);

export default button;