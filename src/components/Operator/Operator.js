import React from 'react';
import './Operator.css';

const operator = props => (
	<div className="operator">
		{ props.children }
	</div>
);

export default operator;