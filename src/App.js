import React, { Component } from 'react';
import axios from 'axios';
import Button from './components/Button/Button';
import Expression from './components/Expression/Expression';
import Operator from './components/Operator/Operator';
import './App.css';

class App extends Component {

	// State used to store all data
	state = {
		input: "",
		prevNumber: "",
		currNumber: "",
		operator: "",
		validClass: "",
		storedEquations: [],
		resultRecieved: false
	};

	// Reset event handler used to reset the current equation
	resetInput = () => {
		if (this.state.resultRecieved) {
			this.state.input = "";
			this.setState({ resultRecieved: false });
		}			
	}

	// Input event handler used to add the inputted number to the input property
	addToInput = value => {
		this.resetInput();
		this.setState({
			input: this.state.input + value
		});
	}

	// Clears all stored data
	clearHandler = () => {
		this.setState({ input: "", operator: "", validClass: "", storedEquations: [] });
	}

	// Make a get request to recieve a random number
	randomNumberHandler = () => {
		axios.get('https://www.random.org/integers/?num=1&min=1&max=100&col=1&base=10&format=plain&rnd=new')
			.then(response => {
				this.resetInput();
				// On response of the request, add the returned number to the input property in the state 
				this.setState({
					input: this.state.input + response.data
				});
			});
	}

	calculationHandler = () => {
		// Spread the input as to not mutate the state
		let spreadInput = [...this.state.input];
		// Join the input so numbers with more than one digit can be concatinated
		let joinInput = spreadInput.join('');

		// Remove any values stored in input and set the states previous number to the last user input
		this.setState({ input: "", prevNumber: joinInput });

		// Add a class if the users inputs an invalid expression
		if (this.state.input === "") {
			this.setState({
				validClass: "invalid"
			});
		}
	}

	addHandler = () => {
		// Disable operator command if a result is currently visible
		if (this.state.resultRecieved) {
			return false;
		}
		this.calculationHandler();
		// Set the states operator to a plus
		this.setState({ operator: "+" });
	}

	subtractHandler = () => {
		// Disable operator command if a result is currently visible
		if (this.state.resultRecieved) {
			return false;
		}		
		this.calculationHandler();
		// Set the states operator to a subtract
		this.setState({ operator: "-" });
	}	

	multiplyHandler = () => {
		// Disable operator command if a result is currently visible
		if (this.state.resultRecieved) {
			return false;
		}		
		this.calculationHandler();
		// Set the states operator to a multiply
		this.setState({ operator: "*" });
	}	

	divideHandler = () => {
		// Disable operator command if a result is currently visible
		if (this.state.resultRecieved) {
			return false;
		}		
		this.calculationHandler();
		// Set the states operator to a divide
		this.setState({ operator: "/" });
	}			

	equalsHandler = () => {
		// Disable equals handler if a result is currently visible or nothing is displayed in the input
		if (this.state.resultRecieved || (this.state.input === "") || (this.state.validClass === "invalid")) {
			return false;
		}		

		// Spread the input as to not mutate the state
		let spreadInput = [...this.state.input];
		let joinInput = spreadInput.join('');
		let result = 0;

		// Conditional calculations for the equals event handler
		if (this.state.operator === "+") {
			result = parseInt(this.state.prevNumber) + parseInt(joinInput);
			this.setState({ 
				input: result
			});
		} else if (this.state.operator === "-") {
			result = parseInt(this.state.prevNumber) - parseInt(joinInput);
			this.setState({ 
				input: result
			});
		} else if (this.state.operator === "*") {
			result = parseInt(this.state.prevNumber) * parseInt(joinInput);
			this.setState({ 
				input: result
			});
		} else if (this.state.operator === "/") {
			result = parseInt(this.state.prevNumber) / parseInt(joinInput)
			this.setState({ 
				input: result
			});
		}

		this.setState({ operator: "", currNumber: joinInput, resultRecieved: true });

		// Concatinate the sum and store it in the equations summary
		let equaltionString = this.state.prevNumber + 
							" " + this.state.operator +
							" " + joinInput + 
							" = " + result.toString() + " | ";

		this.state.storedEquations.push(equaltionString);

		// If the equations summary contains more than 5 sums, remove the oldest one
		if (this.state.storedEquations.length > 5) {
			this.state.storedEquations.shift();
		}

	}

	render() {
		return (
			<div className="calculator">
			  <div className="keypad">
			  	<div className={"expression " + this.state.validClass}>
			  		{ this.state.validClass ? 
			  			<div className="expression-error">Invalid Expression. Press Clear.</div>
			  		 : '' }
			  		<div className="stored-equations">{ this.state.storedEquations }</div>
			  		<Operator>{this.state.operator}</Operator>
			  		<Expression>{this.state.input}</Expression>
			  	</div>
			  	<div className="keypad-row">
			  		<Button clickEventHandler={this.addToInput}>7</Button>
			  		<Button clickEventHandler={this.addToInput}>8</Button>
			  		<Button clickEventHandler={this.addToInput}>9</Button>
			  		<Button clickEventHandler={this.divideHandler}>/</Button>
			  	</div>
			  	<div className="keypad-row">
			  		<Button clickEventHandler={this.addToInput}>4</Button>
			  		<Button clickEventHandler={this.addToInput}>5</Button>
			  		<Button clickEventHandler={this.addToInput}>6</Button>
			  		<Button clickEventHandler={this.multiplyHandler}>*</Button>
			  	</div>
			  	<div className="keypad-row">
			  		<Button clickEventHandler={this.addToInput}>1</Button>
			  		<Button clickEventHandler={this.addToInput}>2</Button>
			  		<Button clickEventHandler={this.addToInput}>3</Button>
			  		<Button clickEventHandler={this.addHandler}>+</Button>
			  	</div>   
			  	<div className="keypad-row">
				  	<div className="button group">
				  		<Button clickEventHandler={this.clearHandler}>Clear</Button>
				  		<Button clickEventHandler={this.randomNumberHandler}>Rand</Button>
				  	</div>
			  		<Button clickEventHandler={this.addToInput}>0</Button>
			  		<Button clickEventHandler={this.equalsHandler}>=</Button>
			  		<Button clickEventHandler={this.subtractHandler}>-</Button>
			  	</div>      	   	      	
			  </div>
			</div>
		);
	}
}

export default App;
