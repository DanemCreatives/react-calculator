The best way to store all the data for the app was to create a state so all data would be accessible via one true source.

I used axios as a way to access the request url. Axios provides a simplistic approach to accessing data and was the best way to implement the random number generator without having to write excessive amounts of code.

Event handlers all use the same calculation handler as they all follow the same methods for storing the numbers. This was necessary to avoid a large portion of the code being reused.

In order to prevent the state being mutated, the useState method was used were possible to avoid state mutation. This is provided by react and is considered a safe way to manipulate data saftely.

I used conditionals as a safeguard for the user inputing invalid expressions.  This means the calculator should always avoid expressions that don't make sense i.e. ++ 1 = NaN.

